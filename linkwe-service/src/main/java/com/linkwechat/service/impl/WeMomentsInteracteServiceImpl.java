package com.linkwechat.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.linkwechat.domain.WeMomentsInteracte;
import com.linkwechat.mapper.WeMomentsInteracteMapper;
import com.linkwechat.service.WeMomentsInteracteService;
import org.springframework.stereotype.Service;

@Service
public class WeMomentsInteracteServiceImpl extends ServiceImpl<WeMomentsInteracteMapper, WeMomentsInteracte> implements WeMomentsInteracteService {

}
